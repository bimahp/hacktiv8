package main

import (
	"database/sql"
	"encoding/json"
	"fmt"
	"html/template"
	"log"
	"net/http"
	"strconv"

	_ "github.com/mattn/go-sqlite3"
	"golang.org/x/crypto/bcrypt"
)

// ResponsePage struct of Response Page
type ResponsePage struct {
	TypeResponse    string
	ResponseMessage string
}

// Article struct of articles
type Article struct {
	ID          int
	Title       string
	Description string
	IsPublished int
}

func main() {
	PORT := ":7077"
	fmt.Println("Port: " + PORT)

	createDefaultUser()
	updateIsLogin(0)

	// Handle Static Files
	fs := http.FileServer(http.Dir("vendor"))
	http.Handle("/vendor/", http.StripPrefix("/vendor/", fs))

	// Routes
	http.HandleFunc("/", home)
	http.HandleFunc("/home", home)
	http.HandleFunc("/about", about)
	http.HandleFunc("/contact-us", contactUs)
	http.HandleFunc("/articles", articles)
	http.HandleFunc("/login", login)
	http.HandleFunc("/logout", logout)

	err := http.ListenAndServe(PORT, nil)
	if err != nil {
		log.Fatal("Error running service: ", err)
	}
}

/*
	HOME
*/
func home(w http.ResponseWriter, r *http.Request) {
	rows := getPublishedArticles()
	listArticles := []Article{}
	for rows.Next() {
		var a Article
		rows.Scan(&a.ID, &a.Title, &a.Description, &a.IsPublished)

		listArticles = append(listArticles, a)
	}

	responses := make(map[string]interface{})
	responses["isLogin"] = checkIsAlreadyLogedIn()
	responses["listArticles"] = listArticles

	t, _ := template.ParseFiles("./view/home.html")
	t.Execute(w, responses)
}

func getPublishedArticles() *sql.Rows {
	database := openDB()
	defer database.Close()

	createTable(database, "articles")

	rows, _ := database.Query("SELECT * FROM articles WHERE is_published=1 ORDER BY 1 DESC")

	return rows
}

/*
	ABOUT
*/
func about(w http.ResponseWriter, r *http.Request) {
	responses := make(map[string]interface{})
	responses["isLogin"] = checkIsAlreadyLogedIn()
	t, _ := template.ParseFiles("./view/about.html")
	t.Execute(w, responses)
}

/*
	CONTACT US
*/
func contactUs(w http.ResponseWriter, r *http.Request) {
	if r.Method == "GET" {
		responses := make(map[string]interface{})
		responses["isLogin"] = checkIsAlreadyLogedIn()

		t, _ := template.ParseFiles("./view/contact_us.html")
		t.Execute(w, responses)
	} else {
		r.ParseForm()

		email := r.Form["email"][0]
		message := r.Form["message"][0]

		var responsePage []ResponsePage
		var isError = false

		if len(email) == 0 {
			isError = true
			responsePage = append(responsePage, ResponsePage{"error", "Email is empty!"})
		}

		if len(message) == 0 {
			isError = true
			responsePage = append(responsePage, ResponsePage{"error", "Message is empty!"})
		}

		if !isError {
			// TODO: Save data to DB
			if insertMessageToAdmin(email, message) {
				responsePage = append(responsePage, ResponsePage{"success", "Thank you for submitting!"})
			} else {
				responsePage = append(responsePage, ResponsePage{"error", "Oops! Something went wrong"})
			}
		}

		responses := make(map[string]interface{})
		responses["isLogin"] = checkIsAlreadyLogedIn()
		responses["responsePage"] = responsePage
		t, _ := template.ParseFiles("./view/contact_us.html")
		t.Execute(w, responses)
	}
}

func insertMessageToAdmin(email string, message string) bool {
	database := openDB()
	defer database.Close()

	statement := createTable(database, "messages")
	statement, _ = database.Prepare("INSERT INTO messages (email, message) VALUES (?, ?)")
	statement.Exec(email, message)
	defer statement.Close()

	// Print to console
	rows, _ := database.Query("SELECT * FROM messages")
	var id int
	fmt.Println("-------- Messages --------")
	for rows.Next() {
		rows.Scan(&id, &email, &message)
		fmt.Println(strconv.Itoa(id) + ": " + email + " " + message)
	}

	return true
}

/*
	ARTICLES
*/
func articles(w http.ResponseWriter, r *http.Request) {
	if !checkIsAlreadyLogedIn() {
		http.Redirect(w, r, "home", http.StatusSeeOther)
	}

	if r.Method == "POST" {
		r.ParseForm()
		action := r.Form["action"][0]

		var responsePage []ResponsePage
		var isError = false

		if len(action) == 0 {
			isError = true
			responsePage = append(responsePage, ResponsePage{"error", "Oops! Something went wrong"})
		}

		var uid string
		if action != "create" {
			uid = r.Form["uid"][0]
			if len(uid) == 0 {
				isError = true
				responsePage = append(responsePage, ResponsePage{"error", "Oops! Something went wrong"})
			}
		}

		if !isError {
			if action == "delete" {
				deleteArticle(uid)
				rows := getAllArticles()
				listArticles := []Article{}
				for rows.Next() {
					var a Article
					rows.Scan(&a.ID, &a.Title, &a.Description, &a.IsPublished)

					listArticles = append(listArticles, a)
				}

				responses := make(map[string]interface{})
				responses["isLogin"] = checkIsAlreadyLogedIn()
				responses["listArticles"] = listArticles

				t, _ := template.ParseFiles("./view/articles.html")
				t.Execute(w, responses)
			} else if action == "publish" {
				updateTo := r.Form["updateTo"][0]
				updateIsPublished(updateTo, uid)

				returnResponse := []string{"success"}
				r, _ := json.Marshal(returnResponse)

				w.Header().Set("Content-Type", "application/json")
				w.Write(r)
			} else if action == "edit" {
				title := r.Form["title"][0]
				desc := r.Form["desc"][0]
				editArticle(title, desc, uid)
				returnResponse := []string{title, desc, uid}
				r, _ := json.Marshal(returnResponse)

				w.Header().Set("Content-Type", "application/json")
				w.Write(r)
			} else if action == "create" {
				title := r.Form["title"][0]
				desc := r.Form["desc"][0]
				insertArticle(title, desc)

				rows := getAllArticles()
				listArticles := []Article{}
				for rows.Next() {
					var a Article
					rows.Scan(&a.ID, &a.Title, &a.Description, &a.IsPublished)

					listArticles = append(listArticles, a)
				}
				responses := make(map[string]interface{})
				responses["isLogin"] = checkIsAlreadyLogedIn()
				responses["listArticles"] = listArticles

				t, _ := template.ParseFiles("./view/articles.html")
				t.Execute(w, responses)
			}
		}
	} else {
		rows := getAllArticles()
		listArticles := []Article{}
		for rows.Next() {
			var a Article
			rows.Scan(&a.ID, &a.Title, &a.Description, &a.IsPublished)

			listArticles = append(listArticles, a)
		}
		responses := make(map[string]interface{})
		responses["isLogin"] = checkIsAlreadyLogedIn()
		responses["listArticles"] = listArticles
		t, _ := template.ParseFiles("./view/articles.html")
		t.Execute(w, responses)
	}
}

func getAllArticles() *sql.Rows {
	database := openDB()
	defer database.Close()

	createTable(database, "articles")

	rows, _ := database.Query("SELECT * FROM articles ORDER BY 1 DESC")

	return rows
}

func insertArticle(title string, description string) bool {
	database := openDB()
	defer database.Close()

	statement := createTable(database, "articles")
	statement, _ = database.Prepare("INSERT INTO articles (title, description, is_published) VALUES (?, ?, ?)")

	statement.Exec(title, description, 1)
	defer statement.Close()

	return true
}

func deleteArticle(id string) bool {
	database := openDB()
	defer database.Close()

	statement := createTable(database, "articles")
	statement, _ = database.Prepare("DELETE FROM articles WHERE id = (?)")

	statement.Exec(id)
	defer statement.Close()

	return true
}

func updateIsPublished(updateTo string, id string) bool {
	database := openDB()
	defer database.Close()

	statement := createTable(database, "articles")
	statement, _ = database.Prepare("UPDATE articles SET is_published = (?) WHERE id = (?)")

	statement.Exec(updateTo, id)
	defer statement.Close()

	return true
}

func editArticle(title string, desc string, id string) bool {
	database := openDB()
	defer database.Close()

	statement := createTable(database, "articles")
	statement, _ = database.Prepare("UPDATE articles SET title = (?), description = (?) WHERE id = (?)")

	statement.Exec(title, desc, id)
	defer statement.Close()

	return true
}

/*
	Login
*/
func login(w http.ResponseWriter, r *http.Request) {
	if checkIsAlreadyLogedIn() {
		http.Redirect(w, r, "home", http.StatusSeeOther)
	}

	if r.Method == "GET" {
		t, _ := template.ParseFiles("./view/login.html")
		t.Execute(w, nil)
	} else {
		r.ParseForm()

		username := r.Form["username"][0]
		password := r.Form["password"][0]

		var responsePage []ResponsePage
		var isError = false

		if len(username) == 0 {
			isError = true
			responsePage = append(responsePage, ResponsePage{"error", "Username is empty!"})
		}

		if len(password) == 0 {
			isError = true
			responsePage = append(responsePage, ResponsePage{"error", "Password is empty!"})
		}

		if !isError {
			// TODO: Save data to DB
			if checkLoginData(username, password) {
				http.Redirect(w, r, "home", http.StatusSeeOther)
			} else {
				http.Redirect(w, r, "login", http.StatusSeeOther)
			}
		}
	}
}

func checkLoginData(username, hashedPassword string) bool {
	password := getPassword(username)

	if password == "NONE" {
		return false
	}

	match := CheckPasswordHash("asdasd", password)

	if match && username == "bima" {
		updateIsLogin(1)
		return true
	}

	return false
}

func getPassword(username string) string {
	database := openDB()
	defer database.Close()
	createTable(database, "users")

	var password string

	err := database.QueryRow("SELECT password FROM users WHERE username='bima' LIMIT 1").Scan(&password)
	switch {
	case err != nil:
		log.Fatal(err)
		return "NONE"
	default:
		return password
	}
}

func updateIsLogin(isLogin int) {
	database := openDB()
	defer database.Close()

	statement := createTable(database, "users")
	statement, _ = database.Prepare("UPDATE users SET is_login = (?) WHERE id = (?)")

	statement.Exec(isLogin, 1)
	defer statement.Close()
}

func checkIsAlreadyLogedIn() bool {
	database := openDB()
	defer database.Close()
	createTable(database, "users")

	var count int

	err := database.QueryRow("SELECT COUNT(1) FROM users WHERE username= 'bima' AND is_login = 1").Scan(&count)
	switch {
	case err != nil:
		log.Fatal(err)
		return false
	default:
		if count <= 0 {
			return false
		}
		return true
	}
}

/*
	LOGOUT
*/
func logout(w http.ResponseWriter, r *http.Request) {
	updateIsLogin(0)
	http.Redirect(w, r, "home", http.StatusSeeOther)
}

/*
	DATABASE
*/
func openDB() *sql.DB {
	database, err := sql.Open("sqlite3", "./hacktive8.db")
	if err != nil {
		panic(err.Error())
	}

	return database
}

func createTable(database *sql.DB, targetTable string) *sql.Stmt {
	var statement *sql.Stmt
	switch targetTable {
	case "messages":
		statement, err := database.Prepare("CREATE TABLE IF NOT EXISTS messages (id INTEGER PRIMARY KEY AUTOINCREMENT, email TEXT, message TEXT)")
		statement.Exec()
		if err != nil {
			panic(err.Error())
		}

	case "articles":
		statement, err := database.Prepare("CREATE TABLE IF NOT EXISTS articles (id INTEGER PRIMARY KEY AUTOINCREMENT, title TEXT, description TEXT, is_published INT)")
		statement.Exec()
		if err != nil {
			panic(err.Error())
		}

	case "users":
		statement, err := database.Prepare("CREATE TABLE IF NOT EXISTS users (id INTEGER PRIMARY KEY AUTOINCREMENT, username TEXT, password TEXT, is_login INT)")
		statement.Exec()
		if err != nil {
			panic(err.Error())
		}
	}

	return statement
}

/*
	ETC
*/
func createDefaultUser() {
	database := openDB()
	defer database.Close()
	statement := createTable(database, "users")

	var count int

	err := database.QueryRow("SELECT COUNT(1) FROM users").Scan(&count)
	switch {
	case err != nil:
		log.Fatal(err)
	default:
		if count <= 0 {
			hash, _ := HashPassword("asdasd")
			statement, _ = database.Prepare("INSERT INTO users (username, password, is_login) VALUES (?, ?, ?)")
			statement.Exec("bima", hash, 0)
			defer statement.Close()
		}
		fmt.Println("Username: bima, Password: asdasd")
	}
}

// HashPassword function to Generate Hashed string
func HashPassword(password string) (string, error) {
	bytes, err := bcrypt.GenerateFromPassword([]byte(password), 14)
	return string(bytes), err
}

// CheckPasswordHash function to Comparare input to Hashed String
func CheckPasswordHash(password, hash string) bool {
	err := bcrypt.CompareHashAndPassword([]byte(hash), []byte(password))
	return err == nil
}
